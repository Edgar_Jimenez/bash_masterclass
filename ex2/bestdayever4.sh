#!/bin/bash
# Getting variables
name=$1
compliment=$2

user=$(whoami)  # variable = output of a command  $()
today_dat=$(date)
whereami=$(pwd)

echo "Good morning $name"
sleep 1
echo "You're looking good today $name !!"
sleep 1
echo "You have the best $compliment i've ever seen $name"
sleep 2

echo "you are currently logged in as $user and you are in the directory $whereami. Also today is $today_dat"