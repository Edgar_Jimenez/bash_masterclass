#!/bin/bash

while read -r line; do
  response=$(curl --write-out "%{http_code}\n" --silent --output /dev/null "$line")
  echo "$line": "$response"
done < lista.txt

# The HTTP 403 Forbidden response status code indicates that the server
# understands the request but refuses to authorize it.