#!/bin/bash
# What Is the curl Command? curl (short for "Client URL") is a
# command line tool that enables data transfer over various network protocols.
curl --write-out "%{http_code}\n" --silent --output /dev/null "$1"
# bash get_http_status.sh https://google.com
# bash get_http_status.sh https://github.com