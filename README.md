# Bash scripting and CI/CD for Dummies

Welcome Data team! During this lab you will explore a bit with bash and devops.
## How this lab works

By the end of this lab you will understand basic bash scripting code and deploy a classification model into production environment using continuos deployment.

first you should clone this repo:

```bash
git clone git@gitlab.com:Edgar_Jimenez/bash_masterclass.git
```
for and Mlops deployment example we will use this other repo:

```bash
git clone git@gitlab.com:Edgar_Jimenez/ci_build_model.git
```
