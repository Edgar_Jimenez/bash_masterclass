echo "Getting latest version of Masterclass from git"

current_dir=$(pwd)

echo $current_dir

get_variable () {
  # read file and assign to a variable
  while read line; do
      eval ${1}="$line"
  done<$2
}

get_variable git_credentials credentials.txt

# echo $git_credentials

if [ ! -d "bash_masterclass" ] ; then
  echo "---->cloning repo"
  # git clone https://<token-name>:<token>@gitlab_url
  git clone $git_credentials@gitlab.com/Edgar_Jimenez/bash_masterclass/ --branch main
  echo "----->Linux requirements"
  bash linux_requirements.sh
  # echo "----->Pulling and automated environment installation in VM is finish"
  echo "\x1B[92m----->Pulling and automated environment installation in VM is finish"
else
  echo "----->pulling code from repo"
  cd ./bash_masterclass
  git checkout dev # change to dev branch
  ls
  cd ./executable
  bash anotherbranchscript.sh
fi


: '
echo "this is a comment line"
 '
